module SimulationGame where

import Player
import GameGraph
import qualified BuchiGame as BG
import ParityGame

import qualified Data.Set as Set

data Side = L | R deriving (Eq, Ord, Show)

data GameVertex = P0 { left :: Vertex, right :: Vertex, turn :: Player, side :: Side }
                | P1 { left :: Vertex, right :: Vertex, turn :: Player, side :: Side }
                deriving (Eq, Ord)

instance Show GameVertex where
  show (P0 l r t s) = "P0 (" ++ show l ++ ", " ++ show r ++ ", " ++ show t ++ ", " ++ show s ++ ")"
  show (P1 l r t s) = "P1 (" ++ show l ++ ", " ++ show r ++ ", " ++ show t ++ ", " ++ show s ++ ")"

type SimulationGameGraph = GameGraph GameVertex
type SimulationGame = BG.BuchiGame GameVertex

isP0 :: GameVertex -> Bool
isP0 (P0 _ _ _ _) = True
isP0 _ = False

isP1 :: GameVertex -> Bool
isP1 x = (not.isP0) x

playerToMove :: ParityGame -> Vertex -> Vertex -> Player
playerToMove g v w | ((owns gr Odd v) && (owns gr Even w)) = Even 
                   | otherwise = Odd
  where gr = graph g
                   
sideToMove :: ParityGame -> Vertex -> Vertex -> Side
sideToMove g v w | owns (graph g) Even v = L
                 | otherwise = R

{-
 - Positions in the direct simulation game are determined as follows:
 -}
createSimulationGameGraph :: ParityGame -> SimulationGameGraph
createSimulationGameGraph pg = GameGraph vEven vOdd edges where
  vs = filter (isConsistentConfig pg) ([P0 v w t s | v <- (Set.toList.vertices.graph) pg, w <- (Set.toList.vertices.graph) pg, t <- [Even, Odd], s <- [L, R]] ++ [P1 v w t s | v <- (Set.toList.vertices.graph) pg, w <- (Set.toList.vertices.graph) pg, t <- [Even, Odd], s <- [L, R]])
  vEven = Set.fromList (filter (\x -> turn x == Even) vs)
  vOdd = Set.fromList (filter (\x -> turn x == Odd) vs)
  edges = Set.fromList (filter (\(x,y) -> isAllowedMove pg x y) [(vv, ww) | vv <- vs, ww <- vs])
  
createSimulationGame :: ParityGame -> SimulationGame
createSimulationGame pg = BG.BuchiGame g b where
  g = createSimulationGameGraph pg
  b = Set.filter (isAcceptingConfig pg) (vertices g)
  
isAcceptingConfig :: ParityGame -> GameVertex -> Bool
isAcceptingConfig g (P0 v w s t) = isConsistentConfig g (P0 v w s t)
isAcceptingConfig g _ = False

isConsistentConfig :: ParityGame -> GameVertex -> Bool
isConsistentConfig g (P0 v w Odd L) = owns (graph g) Even v
isConsistentConfig g (P0 v w Even R) = (owns (graph g) Odd v) && (owns (graph g) Even w)
isConsistentConfig g (P0 v w Odd R) = (owns (graph g) Odd v) && (owns (graph g) Odd w)
isConsistentConfig g (P0 _ _ _ _) = False
isConsistentConfig g (P1 v w Even R) = (owns (graph g) Even w) && (owns (graph g) Even w)
isConsistentConfig g (P1 v w Odd R) = (owns (graph g) Even v) && (owns (graph g) Odd w)
isConsistentConfig g (P1 v w Even L) = owns (graph g) Odd v
isConsistentConfig g (P1 _ _ _ _) = False

-- Precondition: both configurations are consistent    
isAllowedMove :: ParityGame -> GameVertex -> GameVertex -> Bool
isAllowedMove g (P0 v w Odd L) (P1 v' w' t R) = (w == w') && (isSuccessor (graph g) v v')
isAllowedMove g (P0 v w t R) (P1 v' w' Even L) = (v == v') && (isSuccessor (graph g) w w')
isAllowedMove g (P1 v w t R) (P0 v' w' t' s') = (v == v') && (isSuccessor (graph g) w w')
isAllowedMove g (P1 v w t L) (P0 v' w' t' s') = (w == w') && (isSuccessor (graph g) v v')
isAllowedMove _ _ _ = False


computeSimulation :: ParityGame -> Set.Set GameVertex
computeSimulation pg = (BG.solveBuchiGame . createSimulationGame) pg

printSimulationRelation :: Set.Set GameVertex -> Set.Set String
printSimulationRelation gvs = Set.map (\x -> (show (left x) ++ ", " ++ show (right x))) vs where
  vs = Set.filter isP0 gvs
