module ParityGame where

import Player
import GameGraph
import Data.List
import qualified Data.Set as Set

{-
 - A parity game is a pair containing a list of edges, a function assigning priorities to vertices, and
 - a function assigning players to vertices. 
 -}
type Vertex = Int
type Priority = Int

data ParityGame = ParityGame { graph :: GameGraph Vertex
                             , priority :: Vertex -> Priority
                             }
                           
type PGSolverVertex = (Vertex, Player, Priority, [Vertex])

pgSolverVertices :: ParityGame -> [PGSolverVertex]
pgSolverVertices g = [(v, pl v, pr v, ss v) | v <- (Set.toList.vertices.graph) g]
  where pl = (player.graph) g
        pr = priority g
        ss' = (successors.graph) g
        ss = Set.toList.ss'

pgSolverLine :: PGSolverVertex -> String
pgSolverLine (v, pl, pr, ss) = intercalate " " [show v, show pl, show pr, intercalate ", " (map show ss) ++ ";"]

{-
 - Show instance which directly prints the parity game in PGSolver format.
 -}                             
instance Show ParityGame where
  show g = unlines (map pgSolverLine (pgSolverVertices g))

{-
 - The set of vertices with a given priority.
 -}
verticesWithPriority :: ParityGame -> Priority -> Set.Set Vertex
verticesWithPriority g p = Set.filter (\x -> priority g x == p) ((vertices.graph) g)