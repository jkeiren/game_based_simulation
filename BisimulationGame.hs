module BisimulationGame where
  
import LTS
import GameUtil
import qualified Data.Set as Set
import GameGraph
import BuchiGame

{-
 - Bisimulation games for labelled transition systems
 -}
type BisimChallenge = Maybe (Maybe LTSLabel, LTSVertex)
type BisimReward = ()
type BisimGameVertex = GameVertex LTSVertex BisimChallenge ()
type BisimGameGraph = GameGraph BisimGameVertex
type BisimGame = BuchiGame BisimGameVertex

validSpoilerSteps :: LTS -> BisimGameVertex -> [(BisimGameVertex,BisimGameVertex)]
validSpoilerSteps l (Sp v w Nothing ()) = [((Sp v w Nothing ()), (Du v w (Just c) ())) | c <- map edgeChal (outEdges l v)] -- v -a-> v', with challenge (a, v')
                                    ++ [((Sp v w Nothing ()), (Du w v (Just c) ())) | c <- map edgeChal (outEdges l w)] -- w -a-> w', with challenge (a, w')
validSpoilerSteps _ (Du _ _ _ _) = undefined

validDuplicatorSteps :: LTS -> BisimGameVertex -> [(BisimGameVertex,BisimGameVertex)]
validDuplicatorSteps l (Du v w (Just (a, v')) ()) = [((Du v w (Just (a, v')) ()), (Sp v' w' Nothing ())) | w' <- map to (outEdgesP l (\(u,a',_) -> u == w && a == a') w)] 
  -- w -a-> w', provided the challenge was (a, v')
validDuplicatorSteps l (Du _ _ Nothing _) = undefined
validDuplicatorSteps _ (Sp _ _ _ _) = undefined

bisimulationGame :: LTS -> BisimGame
bisimulationGame l@(LTS vsl esl) = BuchiGame g b where
  g = GameGraph (Set.fromList vSs) (Set.fromList vDs) (Set.fromList es)
  vSs = [Sp v w Nothing () | v <- Set.toList vsl, w <- Set.toList vsl]
  vDs = [Du v w (Just ch) () | v <- Set.toList vsl, w <- Set.toList vsl, ch <- map edgeChal (outEdges l v)]
  eSs = concatMap (validSpoilerSteps l) vSs
  eDs = concatMap (validDuplicatorSteps l) vDs
  es = eSs ++ eDs
  b = Set.fromList vSs
  