module BuchiGame where

import Player
import GameGraph
import qualified Data.Set as Set
import Debug.Trace

data BuchiGame vertex = BuchiGame { graph :: GameGraph vertex
                                  , accepting :: Set.Set vertex }

instance (Show vertex) => Show (BuchiGame vertex) where
  show (BuchiGame g a) = "Buchi Game: \n" ++ (show g) ++ "\nAccepting states: " ++ (show a)
                                  
                           
{-
 - Given the Buchi set (accepting), the Buchi objective requires that
 - some state in accepting is visted infinitely often. 
 -}

avoidSetClassical :: (Show vertex, Ord vertex) => GameGraph vertex -> Set.Set vertex -> Set.Set vertex
avoidSetClassical gi bi = trace ("avoidSetClassical gi: " ++ (show gi) ++ " bi: " ++ (show bi) ++ " ri: " ++ (show ri) ++ " tri: " ++ (show tri) ++ " wi': " ++ (show wi')) wi'
  where ri = GameGraph.attractorSet gi Player.Even bi
        tri = (vertices gi) `Set.difference` ri
        wi' = GameGraph.attractorSet gi Player.Odd tri                  

{-
 - We implement the classical interative algorithm, see e.g. [CHP06].
 - Input: a 2-player game graph G = ((S, E), (S1, S2)), and B \subseteq S
 - Output: W \subseteq S
 -}
solveBuchiGame' :: Show vertex => Ord vertex => Int -> GameGraph vertex -> Set.Set vertex -> Set.Set vertex -> Set.Set vertex -> Set.Set vertex -> Set.Set vertex
solveBuchiGame' n gi b si wi w = if Set.null wi' then w else
      (trace ("solveBuchiGame n = " ++ (show n) ++ ", wi': " ++ (show wi') ++ " si': " ++ (show si') ++ "gi': " ++ (show gi') ++ " w': " ++ (show w'))
      (solveBuchiGame' (n+1) gi' b si' wi' w'))
  where
    wi' = avoidSetClassical gi (b `Set.intersection` si)
    si' = si `Set.difference` wi'
    gi' = restrict gi si'
    w' = w `Set.union` wi'

removeDeadendsBG :: (Show vertex, Ord vertex) => BuchiGame vertex -> Player -> (BuchiGame vertex, Set.Set vertex)
removeDeadendsBG bg p = ((BuchiGame g' as'), ds) where
  (g', ds) = removeDeadends (graph bg) p
  as' = (accepting bg) `Set.difference` ds
    
{-
 - G0 := G
 - S0 := S
 - W0 :=- emptyset
 - i := 0
 - repeat
 -   Wi+1 := AvoidSetclassical(Gi, B \cap Si)
 -   Si+1 := Si \setminus Wi+1; Gi+1 := G restricted to Si+1; i := i + 1
 - until Wi = emptyset
 - return W := union of all Wi
 -}
solveBuchiGame :: Show vertex => Ord vertex => BuchiGame vertex -> Set.Set vertex
solveBuchiGame bg = (s0 `Set.difference` (solveBuchiGame' 0 g0 b s0 w0 Set.empty)) `Set.union` desEven
  where (bg', desEven) = removeDeadendsBG bg Even
        (bg'', desOdd) = removeDeadendsBG bg' Odd
        g0 = graph bg''
        b = accepting bg''
        s0 = (vertices g0)
        w0 = Set.empty
        
