module GameUtil where
  
import qualified Data.Set as Set
  
data GameVertex vertex challenge reward = Sp { left :: vertex, right :: vertex, chal :: challenge, rew :: reward}
                                        | Du { left :: vertex, right :: vertex, chal :: challenge, rew :: reward}
                                        deriving (Eq, Ord, Show)
                                 
isSp :: GameVertex vertex challenge reward -> Bool
isSp (Sp _ _ _ _) = True
isSp (Du _ _ _ _) = False

isDu :: GameVertex vertex challenge reward -> Bool
isDu = (not . isSp)
  
printRelation :: (Show vertex) => Set.Set (GameVertex vertex challenge reward) -> Set.Set String
printRelation gvs = Set.map (\x -> ("(" ++ show (left x) ++ ", " ++ show (right x)) ++ ")") vs where
  vs = Set.filter isSp gvs
