module GameGraph where

import Player
import qualified Data.Set as Set
import Data.Function
import Debug.Trace


type Edge vertex = (vertex, vertex)

data GameGraph vertex = GameGraph { evenVertices :: Set.Set vertex
                                  , oddVertices :: Set.Set vertex
                                  , edges :: Set.Set (Edge vertex) }
                                  deriving (Eq, Show)

player :: Eq vertex => GameGraph vertex -> vertex -> Player
player g v | v `elem` evenVertices g = Even
           | otherwise = Odd
           
owns :: Eq vertex => GameGraph vertex -> Player -> vertex -> Bool
owns g p v = (player g v) == p

playerVertices :: GameGraph vertex -> Player -> Set.Set vertex
playerVertices g p | p == Even = evenVertices g
                   | otherwise = oddVertices g
                   
vertices :: Ord vertex => GameGraph vertex -> Set.Set vertex
vertices g = (evenVertices g) `Set.union` (oddVertices g) 

successors :: Ord vertex => GameGraph vertex -> vertex -> Set.Set vertex
successors g v = Set.map snd (Set.filter (\(x,_) -> x == v) (edges g))

isDeadend :: Ord vertex => GameGraph vertex -> vertex -> Bool
isDeadend g v = Set.null (successors g v)

isSuccessor :: Ord vertex => GameGraph vertex -> vertex -> vertex -> Bool
isSuccessor g v w = w `Set.member` successors g v

hasSuccessorIn :: Ord vertex => GameGraph vertex -> Set.Set vertex -> vertex -> Bool
hasSuccessorIn g u v = not (Set.null ((successors g v) `Set.intersection` u))

allSuccessorsIn :: Ord vertex => GameGraph vertex -> Set.Set vertex -> vertex -> Bool
allSuccessorsIn g u v = (successors g v) `Set.isSubsetOf` u

attractorSet :: (Show vertex, Ord vertex) => GameGraph vertex -> Player -> Set.Set vertex -> Set.Set vertex
attractorSet g p u = trace ("Attractor set for player " ++ (show p) ++ " into " ++ (show u) ++ "; next = " ++ (show next)) (if u == next then u else (attractorSet g p next))
  where next = u `Set.union` (Set.filter (hasSuccessorIn g u) (playerVertices g p))
                 `Set.union` (Set.filter (allSuccessorsIn g u) (playerVertices g (opponent p)))
                 
findDeadends :: Ord vertex => GameGraph vertex -> Player -> Set.Set vertex
findDeadends g p = Set.filter (isDeadend g) (playerVertices g p)

removeDeadends :: (Show vertex, Ord vertex) => GameGraph vertex -> Player -> (GameGraph vertex, Set.Set vertex)
removeDeadends g p = (restrict g (vertices g `Set.difference` as), as) where
  ds = findDeadends g p
  as = attractorSet g (opponent p) ds

restrict :: Ord vertex => GameGraph vertex -> Set.Set vertex -> GameGraph vertex
restrict g vs = GameGraph ve vo e where
  ve = evenVertices g `Set.intersection` vs
  vo = oddVertices g `Set.intersection` vs
  e = Set.filter (\(s,t) -> (Set.fromList [s,t] `Set.isSubsetOf` vs)) (edges g)