import Test.HUnit

import qualified Data.Set as Set
import BuchiGame
import Player
import GameGraph
import ParityGame
import BisimulationGame
import BulychevGame
import BranchingBisimulationGame
import SimulationGame
import qualified LTS as LTS

-- Gamegraph of example 1 from http://www.cs.ox.ac.uk/people/luke.ong/personal/publications/RachelBailey_MScdissertation.pdf

gameGraphExample :: GameGraph Int
gameGraphExample = GameGraph ve vo e where
  ve = Set.fromList [0,1,3,4,6,7]
  vo = Set.fromList [2,5]
  e = Set.fromList [(0,1), (0,2), (1,0), (1,2), (2,3), (3,4), (3,5), (4,3), (4,5), (5,6), (6,7), (7,6)]

testOwnsEven = TestCase (assertBool "even owns 3" (owns gameGraphExample Player.Even 3))
testOwnsOdd = TestCase (assertBool "odd owns 5" (owns gameGraphExample Player.Odd 5))
testPlayerVerticesOdd = TestCase (assertEqual "odd vertices" (Set.fromList [2,5]) (oddVertices gameGraphExample))
testPlayerVerticesEven = TestCase (assertEqual "even vertices" (Set.fromList [0,1,3,4,6,7]) (evenVertices gameGraphExample))
testVertices = TestCase (assertEqual "all vertices" (Set.fromList [0..7]) (vertices gameGraphExample))
testSuccessors = TestCase (assertEqual "successors" (Set.fromList [4,5]) (successors gameGraphExample 3))
testIsSuccessor = TestCase (assertBool "isSuccessor" (isSuccessor gameGraphExample 1 0))
testNotIsSuccessor = TestCase (assertBool "not isSuccessor" (not (isSuccessor gameGraphExample 1 3)))
testHasSuccessorIn = TestCase (assertBool "hasSuccessorIn" (hasSuccessorIn gameGraphExample (Set.fromList [1,2,3]) 4))
testAllSuccessorsIn = TestCase (assertBool "allSuccessorsIn" (allSuccessorsIn gameGraphExample (Set.fromList [1,2,3,5,6]) 4))
testNotAllSuccessorsIn = TestCase (assertBool "not allSuccessorsIn" (not (allSuccessorsIn gameGraphExample (Set.fromList [1,2,3]) 4)))

-- Even-attractor into {1,2,5}
testAttractorEven = TestCase (assertEqual "even attractor into {1,2,5} (B)" (Set.fromList [0,1,2,3,4,5]) (GameGraph.attractorSet gameGraphExample Player.Even (Set.fromList [1,2,5])) )
-- Odd-attractor into {6,7}
testAttractorOdd = TestCase (assertEqual "odd attractor into {6,7} (T0)" (Set.fromList [5,6,7]) (GameGraph.attractorSet gameGraphExample Player.Odd (Set.fromList [6,7])))
  
testRestrict = TestCase (assertEqual "restrict 0..4" (GameGraph ve vo e) (GameGraph.restrict gameGraphExample (Set.fromList [0,1,2,3,4]))) where
  ve = Set.fromList [0,1,3,4]
  vo = Set.fromList [2]
  e = Set.fromList [(0,1), (0,2), (1,0), (1,2), (2,3), (3,4), (4,3)]
  
gameGraphTests = TestList [ TestLabel "ownsEven" testOwnsEven
                          , TestLabel "ownsOdd" testOwnsOdd
                          , TestLabel "odd vertices" testPlayerVerticesOdd
                          , TestLabel "even vertices" testPlayerVerticesEven
                          , TestLabel "vertices" testVertices
                          , TestLabel "successors" testSuccessors
                          , TestLabel "is successor" testIsSuccessor
                          , TestLabel "not successor" testNotIsSuccessor
                          , TestLabel "has successor in" testHasSuccessorIn
                          , TestLabel "all successors in" testAllSuccessorsIn
                          , TestLabel "not all successors in" testNotAllSuccessorsIn
                          , TestLabel "attractorEven" testAttractorEven
                          , TestLabel "attractorOdd" testAttractorOdd
                          , TestLabel "restrict" testRestrict
                          ]

-- Example 1 from http://www.cs.ox.ac.uk/people/luke.ong/personal/publications/RachelBailey_MScdissertation.pdf
buchiGameExample :: BuchiGame Int
buchiGameExample = BuchiGame g b where
  g = gameGraphExample
  b = Set.fromList [1,2,5]

testAvoidSetClassicalFull = TestCase (assertEqual "Odd avoids B full graph" (Set.fromList [5,6,7]) (avoidSetClassical gameGraphExample (Set.fromList [1,2,5]) ))
testAvoidSetClassicalSubgraph = TestCase (assertEqual "Odd avoids B subgraph" (Set.fromList [2,3,4]) (avoidSetClassical g b)) where
  g = restrict gameGraphExample (Set.fromList [0..4])
  b = Set.fromList [1,2]
testAvoidSetClassicalSmallGraph = TestCase (assertEqual "Odd avoids B minimal graph" (Set.empty) (avoidSetClassical g b)) where
  g = restrict gameGraphExample (Set.fromList [0,1])
  b = Set.fromList [1]

testBuchi = TestCase (assertEqual "BuchiWinEven (Buchi winning set)" (Set.fromList [0,1]) (solveBuchiGame buchiGameExample))

buchiGameTests = TestList [ TestLabel "avoid set classical on full graph" testAvoidSetClassicalFull
                          , TestLabel "avoid set classical on subgraph" testAvoidSetClassicalSubgraph
                          , TestLabel "avoid set classical on small graph" testAvoidSetClassicalSmallGraph
                          , TestLabel "solve example Buchi game" testBuchi
                          ]

{-
 - Example parity game for testing purposes; Fig 2.2 of the parity game reductions paper.
 -}
exampleParityGame = ParityGame g p where
  g = GameGraph ves vos es
  p v | v == 3 = 0
      | otherwise = 1
  ves = Set.fromList [1,3]
  vos = Set.fromList [0,2]
  es = Set.fromList [(0,2),(1,2),(1,3),(2,2),(3,3)]

testPrio0 = TestCase (assertEqual "Vertices with priority 0" (Set.fromList [3]) (verticesWithPriority exampleParityGame 0))  
testPrio1 = TestCase (assertEqual "Vertices with priority 1" (Set.fromList [0,1,2]) (verticesWithPriority exampleParityGame 1))
testPrio2 = TestCase (assertEqual "Vertices with priority 2" Set.empty (verticesWithPriority exampleParityGame 2))

parityGameTests = TestList [ TestLabel "vertices prio 0" testPrio0
                           , TestLabel "vertices prio 1" testPrio1
                           , TestLabel "vertices prio 2" testPrio2
                           ]

exampleSimulation :: Set.Set GameVertex
exampleSimulation = computeSimulation exampleParityGame

{- LTS -}
exampleLTS = LTS.LTS vs es where
  vs = Set.fromList [0..3]
  es = Set.fromList
       [(0, Just "hack", 0)
       ,(0, Just "eat pizza", 1)
       ,(1, Just "eat pizza", 1)
       ,(2, Just "eat pizza", 1)
       ,(3, Just "hack", 0)
       ,(3, Just "hack", 1)
       ,(3, Just "hack", 2)]
       
bisimGame :: BisimGame
bisimGame = bisimulationGame exampleLTS

exampleLTSbulychevDistinction = LTS.LTS vs es where
  vs = Set.fromList [0..2]
  es = Set.fromList
       [(0, Nothing, 0)
       ,(2, Just "a", 1)
       ]

exampleLTStau = LTS.LTS vs es where
  vs = Set.fromList [0..4]
  es = Set.fromList
       [(0, Just "eat pizza", 4)
       ,(0, Just "hack", 3)
       ,(2, Just "eat pizza", 4)
       ,(1, Just "hack", 3)
       ,(1, Nothing, 2)
       ,(2, Nothing, 1)
       ]

exampleBulychevGame,exampleBulychevGameBroken :: BisimGame
exampleBulychevGameBroken = bulychevGame exampleLTSbulychevDistinction -- interesting observation: result is not an equivalence
exampleBulychevGame = bulychevGame exampleLTStau

exampleBBBulychevGame,exampleBBBulychevGameBroken :: BBisimGame
exampleBBBulychevGameBroken = branchingBisimGame exampleLTSbulychevDistinction False -- interesting observation: result is not an equivalence
exampleBBBulychevGame = branchingBisimGame exampleLTStau False


main :: IO Counts
main = do runTestTT gameGraphTests
          runTestTT buchiGameTests
          runTestTT parityGameTests