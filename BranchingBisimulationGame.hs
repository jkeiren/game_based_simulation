module BranchingBisimulationGame where

import qualified Data.Set as Set
import Data.Maybe
import LTS
import GameUtil
import GameGraph
import BuchiGame
import BisimulationGame

type Reward = Bool

type BBisimGameVertex =  GameVertex LTSVertex BisimChallenge Reward
type BBisimGameGraph = GameGraph BBisimGameVertex
type BBisimGame = BuchiGame BBisimGameVertex

just :: a -> Maybe a
just x = Just x

validSpoilerStepsBranching :: LTS -> BBisimGameVertex -> [(BBisimGameVertex,BBisimGameVertex)]
validSpoilerStepsBranching l (Sp v w ch b) = [((Sp v w ch b), (Du v w (Just ch') b')) | ch' <- map edgeChal (outEdges l v), let b' = (ch /= (Just ch')) && (isJust ch)] -- v -a-> v', with challenge (a, v')
                            ++ [((Sp v w ch b), (Du w v (Just ch') True)) | ch' <- map edgeChal (outEdges l w)] -- w -a-> w', with challenge (a, w')
validSpoilerStepsBranching _ (Du _ _ _ _) = undefined

validDuplicatorStepsBranching :: LTS -> Bool -> BBisimGameVertex -> [(BBisimGameVertex,BBisimGameVertex)]
validDuplicatorStepsBranching l ed (Du v w (Just (a, v')) b) = 
     [((Du v w (Just (a, v')) b), (Sp v' w' Nothing True)) | w' <- map to (outEdgesP l (\(u,a',_) -> u == w && a == a') w)] 
  ++ [((Du v w (Just (a, v')) b), (Sp v w' (Just (a, v')) (not ed))) | w' <- map to (outEdgesP l (\(u, a, _) -> u == w && a == Nothing) w)] -- v --a--> v' mimicked by taking a tau step
  ++ [((Du v w (Just (a, v')) b), (Sp v' w Nothing True)) | a == Nothing] -- v --tau-- v' mimicked by staying in v
validDuplicatorStepsBranching _ _ (Du _ _ Nothing _) = undefined -- Explicit assumption that Duplicator always has a challenge
validDuplicatorStepsBranching _ _ (Sp _ _ _ _) = undefined
  
branchingBisimGame :: LTS -> Bool -> BBisimGame
branchingBisimGame l@(LTS vsl esl) explicitDivergences = BuchiGame g b where
  g = GameGraph (Set.fromList vSs) (Set.fromList vDs) (Set.fromList es)
  vSs = [Sp v w ch b | v <- Set.toList vsl, w <- Set.toList vsl, ch <- (map (just.edgeChal) (outEdges l v)) ++ [Nothing], b <- [False, True]]
  vDs = [Du v w ch b | v <- Set.toList vsl, w <- Set.toList vsl, ch <- (map (just.edgeChal) (outEdges l v)) ++ [Nothing], b <- [False, True]]
  eSs = concatMap (validSpoilerStepsBranching l) vSs
  eDs = concatMap (validDuplicatorStepsBranching l explicitDivergences) vDs
  es = eSs ++ eDs
  b = Set.fromList vSs