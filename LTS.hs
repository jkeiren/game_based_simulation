module LTS where

import Data.List
import qualified Data.Set as Set

{-
 - A parity game is a pair containing a list of edges, a function assigning priorities to vertices, and
 - a function assigning players to vertices. 
 -}
type LTSVertex = Int
type LTSLabel = String
type LTSEdge = (LTSVertex, Maybe LTSLabel, LTSVertex) -- label tau (internal action) is coded as none, other labels are Just label

to :: LTSEdge -> LTSVertex
to (_,_,w) = w

dropSource :: LTSEdge -> (Maybe LTSLabel, LTSVertex)
dropSource (u,a,v) = (a,v)
edgeChal :: LTSEdge -> (Maybe LTSLabel, LTSVertex)
edgeChal = dropSource -- TODO: remove

data LTS = LTS { vertices :: Set.Set LTSVertex
               , edges :: Set.Set LTSEdge
               } deriving (Eq, Show)
               
outEdgesP :: LTS -> (LTSEdge -> Bool) -> LTSVertex -> [LTSEdge]
outEdgesP l p v = filter p (Set.toList (LTS.edges l))

outEdges :: LTS -> LTSVertex -> [LTSEdge]
outEdges l v = outEdgesP l (\(u,_,_) -> u == v) v