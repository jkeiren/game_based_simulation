module BulychevGame where

import qualified Data.Set as Set

import LTS
import GameUtil
import GameGraph
import BuchiGame
import BisimulationGame

validDuplicatorStepsBulychev :: LTS -> BisimGameVertex -> [(BisimGameVertex,BisimGameVertex)]
validDuplicatorStepsBulychev l c@(Du v w (Just (a, v')) ()) = validDuplicatorSteps l c
  ++ [(c, (Sp v w' Nothing ())) | w' <- map to (outEdgesP l (\(u, a, _) -> u == w && a == Nothing) w)] -- v --a--> v' mimicked by taking a tau step
  ++ [(c, (Sp v' w Nothing ())) | a == Nothing] -- v --tau-- v' mimicked by staying in v
validDuplicatorStepsBulychev _ (Du _ _ Nothing _) = undefined
validDuplicatorStepsBulychev _ (Sp _ _ _ _) = undefined
  
bulychevGame :: LTS -> BisimGame
bulychevGame l@(LTS vsl esl) = BuchiGame g b where
  g = GameGraph (Set.fromList vSs) (Set.fromList vDs) (Set.fromList es)
  vSs = [Sp v w Nothing () | v <- Set.toList vsl, w <- Set.toList vsl]
  vDs = [Du v w (Just ch) () | v <- Set.toList vsl, w <- Set.toList vsl, ch <- map edgeChal (outEdges l v)]
  eSs = concatMap (validSpoilerSteps l) vSs
  eDs = concatMap (validDuplicatorStepsBulychev l) vDs
  es = eSs ++ eDs
  b = Set.fromList vSs