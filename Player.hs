module Player where

data Player = Even | Odd
  deriving (Eq, Ord)

instance Show Player where
  show p | p == Even = "0"
         | otherwise = "1"

opponent :: Player -> Player
opponent Even = Odd
opponent Odd = Even